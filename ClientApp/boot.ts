import './css/site.css';
import 'bootstrap';
import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

const routes = [
    { path: '/', component: require('./components/home/home.vue.html') },
    { path: '/counter', component: require('./components/counter/counter.vue.html') },
    { path: '/fetchdata', component: require('./components/fetchdata/fetchdata.vue.html') },
    { path: '/accountlist', component: require('./components/accountlist/accountlist.vue.html') },
    { path: '/users', component: require('./components/users/users.vue.html') },
    { path: '/posts', component: require('./components/posts/posts.vue.html') },
    { path: '/userposts', component: require('./components/userposts/userposts.vue.html') },
    { path: '/chatc', component: require('./components/chat/chat.vue.html') },
    { path: '/books', component: require('./components/books/books.vue.html') }
];

new Vue({
    el: '#app-root',
    router: new VueRouter({ mode: 'history', routes: routes }),
    render: h => h(require('./components/app/app.vue.html'))
});
