import { Vue, Component } from 'vue-property-decorator';

interface Account {
    id: string;
    name: string;
    password: string;
    email: string;
    createTime: Date;
    webhookEndpoint: string | null;
    status: string;
    editing?: boolean;
}

@Component
export default class AccountList extends Vue {
    accounts: Account[] = [];

    newAccount: Partial<Account> = {
        name: '',
        email: '',
        password: ''
    };

    created() {
        this.fetchAccounts();
    }

    async fetchAccounts() {
        try {
            const response = await fetch('/api/account');
            if (response.ok) {
                this.accounts = await response.json();
                // Initialize editing state for each account
                this.accounts.forEach(account => {
                    Vue.set(account, 'editing', false);
                });
            } else {
                console.error('Failed to fetch accounts:', response.statusText);
            }
        } catch (error) {
            console.error('Error fetching accounts:', error);
        }
    }

    async addAccount() {
        try {
            const response = await fetch('/api/account', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(this.newAccount)
            });
            if (response.ok) {
                this.newAccount = { name: '', email: '', password: '' };
                this.fetchAccounts();
            } else {
                console.error('Failed to add account:', response.statusText);
            }
        } catch (error) {
            console.error('Error adding account:', error);
        }
    }

    startEditing(account: Account) {
        Vue.set(account, 'editing', true);
    }

    async updateAccount(account: Account) {
        try {
            const response = await fetch(`/api/account/${account.id}`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(account)
            });
            if (response.ok) {
                Vue.set(account, 'editing', false);
                this.fetchAccounts();
            } else {
                console.error('Failed to update account:', response.statusText);
            }
        } catch (error) {
            console.error('Error updating account:', error);
        }
    }

    async deleteAccount(id: string) {
        try {
            const response = await fetch(`/api/account/${id}`, {
                method: 'DELETE'
            });
            if (response.ok) {
                this.fetchAccounts();
            } else {
                console.error('Failed to delete account:', response.statusText);
            }
        } catch (error) {
            console.error('Error deleting account:', error);
        }
    }
}
