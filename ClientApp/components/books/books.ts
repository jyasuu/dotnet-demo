
import Vue from 'vue';
import { Component } from 'vue-property-decorator';

interface Book {
  id: string;
  title: string;
  author: string;
  pages: number;
}

interface NewBook {
  title: string;
  author: string;
  pages: number;
}

@Component
export default class BooksComponent extends Vue {
  books: Book[] = [];
  newBook: NewBook = {
    title: '',
    author: '',
    pages: 0,
  };

  mounted() {
    this.fetchBooks();
  }

  fetchBooks() {
    const query = `
      {
        books {
          id
          title
          author
          pages
        }
      }
    `;

    fetch('graphql/books', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ query }),
    })
      .then(response => response.json())
      .then(data => {
        this.books = data.data.books;
      })
      .catch(error => {
        console.error(error);
      });
  }

  createBook() {
    const mutation = `
      mutation($book: BookInput!) {
        createBook(book: $book) {
          id
          title
          author
          pages
        }
      }
    `;

    const variables = {
      book: this.newBook,
    };

    fetch('graphql/books', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ query: mutation, variables }),
    })
      .then(response => response.json())
      .then(data => {
        this.books.push(data.data.createBook);
        this.newBook = { title: '', author: '', pages: 0 };
      })
      .catch(error => {
        console.error(error);
      });
  }

  updateBook(id: string) {
    const mutation = `
      mutation($id: ID!, $book: BookInput!) {
        updateBook(id: $id, book: $book) {
          id
          title
          author
          pages
        }
      }
    `;

    const variables = {
      id,
      book: { title: 'Updated Title', author: 'Updated Author', pages: 123 },
    };

    fetch('graphql/books', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ query: mutation, variables }),
    })
      .then(response => response.json())
      .then(data => {
        const index = this.books.findIndex(book => book.id === id);
        if (index !== -1) {
          this.books.splice(index, 1, data.data.updateBook);
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  deleteBook(id: string) {
    const mutation = `
      mutation($id: ID!) {
        deleteBook(id: $id)
      }
    `;

    const variables = {
      id,
    };

    fetch('graphql/books', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ query: mutation, variables }),
    })
      .then(response => response.json())
      .then(data => {
        this.books = this.books.filter(book => book.id !== id);
      })
      .catch(error => {
        console.error(error);
      });
  }
}