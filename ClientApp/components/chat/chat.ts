import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import { startConnection, sendMessage, onReceiveMessage } from '../../services/signalrService';

@Component
export default class ChatComponent extends Vue {
  user: string = '';
  message: string = '';
  messages: string[] = [];

  // Method to send messages
  sendMessage(): void {
    if (this.user && this.message) {
      sendMessage(this.user, this.message);
      this.message = ''; // Clear message input
    }
  }

  // Lifecycle hook to start SignalR connection and set up message reception
  mounted(): void {
    startConnection();
    onReceiveMessage((user: string, message: string) => {
      this.messages.push(`${user}: ${message}`);
    });
  }
}
