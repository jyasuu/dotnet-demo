import Vue from 'vue';
import { Component } from 'vue-property-decorator';

interface Post {
    id: number;
    title: string;
    content: string;
}

@Component
export default class PostsComponent extends Vue {
    posts: Post[] = [];

    mounted() {
        const query = `
            {
                posts {
                    id
                    title
                    content
                }
            }
        `;
        
        fetch('graphql/blog', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ query }),
        })
        .then(response => response.json())
        .then(data => {
            this.posts = data.data.posts;
        })
        .catch(error => {
            console.error(error);
        });
    }
}
