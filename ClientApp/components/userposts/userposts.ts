import Vue from 'vue';
import { Component, Prop } from 'vue-property-decorator';

interface Post {
    id: number;
    title: string;
    content: string;
    comments: Comment[];
}

interface Comment {
    id: number;
    content: string;
    user: User;
}

interface User {
    id: number;
    username: string;
    email: string;
}

@Component
export default class UserPostsComponent extends Vue {
    posts: Post[] = [];

    mounted() {
        const query = `
            {
                posts {
                    id
                    title
                    content
                    comments {
                        id
                        content
                        user {
                            username
                        }
                    }
                }
            }
        `;

        fetch('graphql/blog', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ query }),
        })
        .then(response => response.json())
        .then(data => {
            this.posts = data.data.posts;
        })
        .catch(error => {
            console.error(error);
        });
    }
}
