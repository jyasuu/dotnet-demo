import Vue from 'vue';
import { Component } from 'vue-property-decorator';

interface User {
    id: number;
    username: string;
    email: string;
}

@Component
export default class UsersComponent extends Vue {
    users: User[] = [];

    mounted() {
        const query = `
            {
                users {
                    id
                    username
                    email
                }
            }
        `;
        
        fetch('graphql/blog', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ query }),
        })
        .then(response => response.json())
        .then(data => {
            this.users = data.data.users;
        })
        .catch(error => {
            console.error(error);
        });
    }
}
