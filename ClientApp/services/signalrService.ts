
import * as signalR from "@microsoft/signalr";

const connection = new signalR.HubConnectionBuilder()
    .withUrl("chathub") // URL of the SignalR Hub
    .build();

export function startConnection(): void {
    connection.start().catch(err => console.error("SignalR connection error:", err));
}

export function sendMessage(user: string, message: string): void {
    connection.invoke("Send", user, message)
        .catch(err => console.error("Error sending message:", err));
}

export function onReceiveMessage(callback: (user: string, message: string) => void): void {
    connection.on("broadcastMessage", (user: string, message: string) => {
        callback(user, message);
    });
}
