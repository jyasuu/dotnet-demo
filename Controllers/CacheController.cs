// Controllers/CacheController.cs
using dotnet_demo.Data;
using dotnet_demo.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;


namespace dotnet_demo.Controllers
{


    [Route("api/[controller]")]
    public class CacheController : Controller
    {
        private readonly CacheService _cacheService;

        public CacheController(CacheService cacheService)
        {
            _cacheService = cacheService;
        }

        [HttpGet("set")]
        public async Task<IActionResult> SetCache()
        {
            var testData = new TestData { Id = "1", Name = "Test", Value = 123 };
            await _cacheService.SetCacheAsync("test_data", testData, TimeSpan.FromMinutes(5));
            return Json(new { message = "Data cached" });
        }

        [HttpGet]
        public async Task<IActionResult> GetCache()
        {
            var cachedData = await _cacheService.GetCacheAsync<TestData>("test_data");
            if (cachedData == null)
            {
                return Json(new { message = "No data found in cache" });
            }
            return Json(cachedData);
        }
    }

}