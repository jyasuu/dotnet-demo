// Data/MongoDbContext.cs
using dotnet_demo.Models;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace dotnet_demo.Data
{
    public class MongoDbContext
    {
        private readonly IMongoDatabase _database;

        public MongoDbContext(string connectionString, string databaseName)
        {
            var client = new MongoClient(connectionString);
            _database = client.GetDatabase(databaseName);
        }

        public IMongoCollection<Book> Books => _database.GetCollection<Book>("Books");
    }

}