

namespace dotnet_demo.Data
{
    public class TestData
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int Value { get; set; }
    }

}
