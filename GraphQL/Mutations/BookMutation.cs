// GraphQL/Mutations/BookMutation.cs
using dotnet_demo.Data;
using dotnet_demo.GraphQL.Types;
using dotnet_demo.Models;
using GraphQL;
using GraphQL.Types;
using MongoDB.Driver;

namespace dotnet_demo.GraphQL.Mutations{

    public class BookMutation : ObjectGraphType
    {
        public BookMutation(MongoDbContext dbContext)
        {
            Field<BookType>(
                "createBook",
                arguments: new QueryArguments(new QueryArgument<NonNullGraphType<BookInputType>> { Name = "book" }),
                resolve: context =>
                {
                    var book = context.GetArgument<Book>("book");
                    dbContext.Books.InsertOne(book);
                    return book;
                }
            );

            Field<BookType>(
                "updateBook",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "id" },
                    new QueryArgument<NonNullGraphType<BookInputType>> { Name = "book" }
                ),
                resolve: context =>
                {
                    var id = context.GetArgument<string>("id");
                    var book = context.GetArgument<Book>("book");
                    dbContext.Books.ReplaceOne(b => b.Id == id, book);
                    return book;
                }
            );

            Field<StringGraphType>(
                "deleteBook",
                arguments: new QueryArguments(new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "id" }),
                resolve: context =>
                {
                    var id = context.GetArgument<string>("id");
                    dbContext.Books.DeleteOne(book => book.Id == id);
                    return id;
                }
            );
        }
    }

}