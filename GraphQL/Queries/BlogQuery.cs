using GraphQL.Types;
using dotnet_demo.Models;
using Microsoft.EntityFrameworkCore;
using dotnet_demo.GraphQL.Types;

namespace dotnet_demo.GraphQL.Queries
{
    public class BlogQuery : ObjectGraphType
    {
        public BlogQuery(PostgresContext db)
        {
        Field<ListGraphType<UserType>>(
            "users",
            resolve: context => db.User.Include(u => u.Post).Include(u => u.Comment)
        );

        Field<ListGraphType<PostType>>(
            "posts",
            resolve: context => db.Post.Include(p => p.User).Include(p => p.Comment)
        );

        Field<ListGraphType<CommentType>>(
            "comments",
            resolve: context => db.Comment.Include(c => c.User).Include(c => c.Post)
        );
        }
    }


}
