// GraphQL/Queries/BookQuery.cs
using dotnet_demo.Data;
using dotnet_demo.GraphQL.Types;
using GraphQL;
using GraphQL.Types;
using MongoDB.Driver;

namespace dotnet_demo.GraphQL.Queries
{

    public class BookQuery : ObjectGraphType
    {
        public BookQuery(MongoDbContext dbContext)
        {
            Field<ListGraphType<BookType>>(
                "books",
                resolve: context => dbContext.Books.Find(_ => true).ToListAsync()
            );

            Field<BookType>(
                "book",
                arguments: new QueryArguments(new QueryArgument<IdGraphType> { Name = "id" }),
                resolve: context =>
                {
                    var id = context.GetArgument<string>("id");
                    return dbContext.Books.Find(book => book.Id == id).FirstOrDefaultAsync();
                }
            );
        }
    }

}