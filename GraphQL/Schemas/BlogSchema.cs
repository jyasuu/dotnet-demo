using System;
using dotnet_demo.GraphQL.Queries;
using GraphQL;
using GraphQL.Types;
using Microsoft.Extensions.DependencyInjection;

namespace dotnet_demo.GraphQL.Schemas
{
    public class BlogSchema : Schema
    {
        public BlogSchema(IServiceProvider provider) : base(provider)
        {
            Query = provider.GetRequiredService<BlogQuery>();
        }
    }

    
}