// GraphQL/Schemas/BookSchema.cs
using System;
using dotnet_demo.GraphQL.Mutations;
using dotnet_demo.GraphQL.Queries;
using GraphQL;
using GraphQL.Types;
using GraphQL.Utilities;

namespace dotnet_demo.GraphQL.Schemas
{
    public class BookSchema : Schema
    {
        public BookSchema(IServiceProvider provider) : base(provider)
        {
            Query = provider.GetRequiredService<BookQuery>();
            Mutation = provider.GetRequiredService<BookMutation>();
        }
    }

}