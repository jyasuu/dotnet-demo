// GraphQL/Types/BookInputType.cs
using GraphQL.Types;

namespace dotnet_demo.GraphQL.Types
{
    public class BookInputType : InputObjectGraphType
    {
        public BookInputType()
        {
            Name = "BookInput";
            Field<NonNullGraphType<StringGraphType>>("title");
            Field<NonNullGraphType<StringGraphType>>("author");
            Field<NonNullGraphType<IntGraphType>>("pages");
        }
    }

}