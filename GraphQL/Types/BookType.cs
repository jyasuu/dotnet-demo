// GraphQL/Types/BookType.cs
using dotnet_demo.Models;
using GraphQL.Types;

namespace dotnet_demo.GraphQL.Types
{
    public class BookType : ObjectGraphType<Book>
    {
        public BookType()
        {
            Field(x => x.Id, type: typeof(IdGraphType)).Description("The ID of the Book.");
            Field(x => x.Title).Description("The title of the book.");
            Field(x => x.Author).Description("The author of the book.");
            Field(x => x.Pages).Description("The number of pages in the book.");
        }
    }

}