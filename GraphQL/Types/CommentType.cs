using GraphQL.Types;
using dotnet_demo.Models;

namespace dotnet_demo.GraphQL.Types
{
    public class CommentType : ObjectGraphType<Comment>
    {
        public CommentType()
        {
            Field(x => x.Id).Description("Comment ID");
            Field(x => x.Content).Description("Comment Content");
            Field(x => x.CreatedAt).Description("Created At");
            
            Field<UserType>("user", resolve: context => context.Source.User);
            Field<PostType>("post", resolve: context => context.Source.Post);
        }
    }
}
