using GraphQL.Types;
using dotnet_demo.Models;

namespace dotnet_demo.GraphQL.Types
{
    public class PostType : ObjectGraphType<Post>
    {
        public PostType()
        {
            Field(x => x.Id).Description("Post ID");
            Field(x => x.Title).Description("Post Title");
            Field(x => x.Content).Description("Post Content");
            Field(x => x.CreatedAt).Description("Created At");
            
            Field<UserType>("user", resolve: context => context.Source.User);
            Field<ListGraphType<CommentType>>("comments", resolve: context => context.Source.Comment);
        }
    }
}
