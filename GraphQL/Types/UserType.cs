using GraphQL.Types;
using dotnet_demo.Models;

namespace dotnet_demo.GraphQL.Types
{
    public class UserType : ObjectGraphType<User>
    {
        public UserType()
        {
            Field(x => x.Id).Description("User ID");
            Field(x => x.Username).Description("Username");
            Field(x => x.Email).Description("Email");
            Field(x => x.CreatedAt).Description("Created At");
            
            Field<ListGraphType<PostType>>("posts", resolve: context => context.Source.Post);
            Field<ListGraphType<CommentType>>("comments", resolve: context => context.Source.Comment);
        }
    }
}
