
using dotnet_demo.Services;
using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Threading.Tasks;

public class SampleJob : IJob
{
    private readonly ILogger _logger;
    private readonly RabbitMQService _rabbitMQService;

    public SampleJob(ILogger<SampleJob> logger,RabbitMQService rabbitMQService)
    {
        _logger = logger;
        _rabbitMQService = rabbitMQService;
    }

    public Task Execute(IJobExecutionContext context)
    {
        _logger.LogInformation("About job execute at {DT}", 
            DateTime.UtcNow.ToLongTimeString());

        _rabbitMQService.PublishMessage(String.Format($"About job execute at {DateTime.UtcNow.ToLongTimeString()}" ));
            
        return Task.CompletedTask;

    }

}