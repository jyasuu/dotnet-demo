
using Microsoft.Extensions.Hosting;
using RabbitMQ.Client;
using System.Text;
using System.Threading;
using System.Threading.Tasks;



namespace dotnet_demo.Services
{

    public class HostedRabbitMQConsumerService : IHostedService
    {
        private readonly RabbitMQConsumerService _consumerService;

        public HostedRabbitMQConsumerService(RabbitMQConsumerService consumerService)
        {
            _consumerService = consumerService;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            // The consumer starts automatically in the constructor
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            // Cleanup resources if necessary
            return Task.CompletedTask;
        }
    }

}