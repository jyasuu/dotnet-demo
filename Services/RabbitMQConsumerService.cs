using dotnet_demo.Hubs;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;



namespace dotnet_demo.Services
{

    public class RabbitMQConsumerService
    {
        private readonly IConnection _connection;
        private readonly IModel _channel;

    private readonly IHubContext<ChatHub> _hubContext;

        public RabbitMQConsumerService(ILogger<RabbitMQConsumerService> logger,IHubContext<ChatHub> hubContext)
        {

            _hubContext = hubContext;
            var factory = new ConnectionFactory() { HostName = "localhost" }; // Update with your RabbitMQ server settings
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();

            string exchangeName = "myFanoutExchange";
            _channel.ExchangeDeclare(exchange: exchangeName, type: ExchangeType.Fanout);

            // Declare queues
            _channel.QueueDeclare(queue: "Queue1", durable: false, exclusive: false, autoDelete: false, arguments: null);
            _channel.QueueDeclare(queue: "Queue2", durable: false, exclusive: false, autoDelete: false, arguments: null);

            // Bind queues to the fanout exchange
            _channel.QueueBind(queue: "Queue1", exchange: exchangeName, routingKey: "");
            _channel.QueueBind(queue: "Queue2", exchange: exchangeName, routingKey: "");

            _channel.QueueDeclare(queue: "sampleQueue",
                                durable: false,
                                exclusive: false,
                                autoDelete: false,
                                arguments: null);
            {
                var consumer = new EventingBasicConsumer(_channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body.ToArray();
                    var message = Encoding.UTF8.GetString(body);
                    logger.LogInformation($"Received message: {message}");
                    _hubContext.Clients.All.SendAsync("broadcastMessage","GA",$"Received message: {message}");

                    // Handle the message (e.g., update the database, trigger another process, etc.)
                };
                _channel.BasicConsume(queue: "sampleQueue",
                                    autoAck: true,
                                    consumer: consumer);
            }
            {
                var consumer = new EventingBasicConsumer(_channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body.ToArray();
                    var message = Encoding.UTF8.GetString(body);
                    logger.LogInformation($"Received message: {message}");
                    _hubContext.Clients.All.SendAsync("broadcastMessage","Queue1 GA",$"Received message: {message}");

                    // Handle the message (e.g., update the database, trigger another process, etc.)
                };
                _channel.BasicConsume(queue: "Queue1",
                                    autoAck: true,
                                    consumer: consumer);
            }
            {
                var consumer = new EventingBasicConsumer(_channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body.ToArray();
                    var message = Encoding.UTF8.GetString(body);
                    logger.LogInformation($"Received message: {message}");
                    _hubContext.Clients.All.SendAsync("broadcastMessage","Queue2 GA",$"Received message: {message}");

                    // Handle the message (e.g., update the database, trigger another process, etc.)
                };
                _channel.BasicConsume(queue: "Queue2",
                                    autoAck: true,
                                    consumer: consumer);
            }
        }
    }


}