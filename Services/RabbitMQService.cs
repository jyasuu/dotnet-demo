
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using System.Text;



namespace dotnet_demo.Services
{

    public class RabbitMQService
    {
        private readonly IConnection _connection;
        private readonly IModel _channel;
        private readonly ILogger _logger;

        public RabbitMQService(ILogger<RabbitMQService> logger)
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _logger = logger;
        }

        public void PublishMessage(string message)
        {

            _channel.QueueDeclare(queue: "sampleQueue", durable: false, exclusive: false, autoDelete: false, arguments: null);
            var body = Encoding.UTF8.GetBytes(message);

            _channel.BasicPublish(exchange: "", routingKey: "sampleQueue", basicProperties: null, body: body);


            // Declare a fanout exchange
            string exchangeName = "myFanoutExchange";
            _channel.ExchangeDeclare(exchange: exchangeName, type: ExchangeType.Fanout);

            // Publish the message to the fanout exchange
            _channel.BasicPublish(exchange: exchangeName,
                                 routingKey: "", // Empty routing key for fanout exchange
                                 basicProperties: null,
                                 body: body);

            _logger.LogInformation(message);
        }
    }

}