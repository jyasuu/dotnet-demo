using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dotnet_demo.Models;
using GraphQL.Types;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Npgsql;
using dotnet_demo.GraphQL.Types;
using dotnet_demo.GraphQL.Queries;
using dotnet_demo.GraphQL.Schemas;
using GraphQL.Server.Ui.Playground;
using GraphQL.Server;
using Quartz;
using dotnet_demo.Services;
using dotnet_demo.Hubs;
using dotnet_demo.Data;
using dotnet_demo.GraphQL.Mutations;


namespace dotnet_demo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            
            var connectionString = Configuration["PostgresSQL:ConnectionString"];
            var dbPassword = Configuration["PostgresSQL:DbPassword"];
            var builder = new NpgsqlConnectionStringBuilder(connectionString)
            {
                Password = dbPassword
            };
            services.AddDbContext<PostgresContext>(options => options.UseNpgsql(builder.ConnectionString));

            // Register GraphQL types and schema
            services.AddScoped<UserType>();
            services.AddScoped<PostType>();
            services.AddScoped<CommentType>();
            services.AddScoped<BlogQuery>();
            services.AddScoped<ISchema, BlogSchema>();
            services.AddScoped<BlogSchema>();
            services.AddSingleton<CacheService>();

            // Configure GraphQL
            services.AddGraphQL(options =>
            {
                options.EnableMetrics = false;
            })
            .AddNewtonsoftJson(deserializerSettings => { }, serializerSettings => { })
            .AddGraphTypes(ServiceLifetime.Scoped);


            services.AddQuartz(q =>
            {
                q.UseMicrosoftDependencyInjectionScopedJobFactory();
                var jobKey = new JobKey("SampleJob");
                q.AddJob<SampleJob>(opts => opts.WithIdentity(jobKey));
                q.AddTrigger(opts => opts
                    .ForJob(jobKey)
                    .WithIdentity("SampleJob-trigger")
                    .WithCronSchedule("0/5 * * * * ?")); // Every 5 seconds
            });

            
            // ASP.NET Core hosting
            services.AddQuartzServer(options =>
            {
                // when shutting down we want jobs to complete gracefully
                options.WaitForJobsToComplete = true;
                
            });

            // services.AddQuartzHostedService(q => q.WaitForJobsToComplete = true);



            services.AddDistributedRedisCache(options =>
            {
                options.Configuration = "localhost:6379"; // Your Redis server configuration
                options.InstanceName = "SampleInstance";
            });


            services.AddSignalR();

            services.AddSingleton<RabbitMQService>();
            services.AddSingleton<RabbitMQConsumerService>();

            services.AddHostedService<HostedRabbitMQConsumerService>();


            services.AddSingleton<MongoDbContext>(sp =>
                new MongoDbContext("mongodb://admin:adminpw@localhost:27017", "mydb"));


            
            services.AddScoped<BookType>();
            services.AddScoped<BookInputType>();
            services.AddScoped<BookQuery>();
            services.AddScoped<BookMutation>();
            services.AddScoped<ISchema, BookSchema>();
            services.AddScoped<BookSchema>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
                {
                    HotModuleReplacement = true
                });
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseGraphQL<BlogSchema>("/graphql/blog");
            app.UseGraphQL<BookSchema>("/graphql/books");
            app.UseGraphQLPlayground(new GraphQLPlaygroundOptions
            {
                GraphQLEndPoint = "/graphql",
                Path = "/ui/playground",
            });
            
            app.UseSignalR(routes =>
            {
                routes.MapHub<ChatHub>("/chathub");
            });

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                routes.MapSpaFallbackRoute(
                    name: "spa-fallback",
                    defaults: new { controller = "Home", action = "Index" });
            });


        }
    }
}
