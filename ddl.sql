

-- public.account definition

-- Drop table

-- DROP TABLE public.account;

CREATE TABLE public.account (
	id varchar NOT NULL DEFAULT gen_random_uuid(),
	"name" varchar NOT NULL,
	"password" varchar NOT NULL,
	email varchar NOT NULL,
	create_time timestamp NOT NULL DEFAULT now(),
	webhook_endpoint varchar NULL,
	"status" varchar NOT NULL DEFAULT 'Y'::character varying,
    "version" INT NOT NULL DEFAULT 1,
	CONSTRAINT account_pk PRIMARY KEY (id),
	CONSTRAINT account_un UNIQUE (name)
);




CREATE TABLE public.person (
	id varchar NOT NULL DEFAULT gen_random_uuid(),
	firstname varchar NOT NULL,
	lastname varchar NOT NULL
);


CREATE TABLE public.user (
    id SERIAL PRIMARY KEY,
    username VARCHAR(50) UNIQUE NOT NULL,
    email VARCHAR(100) UNIQUE NOT NULL,
    password VARCHAR(100) NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW()
);

CREATE TABLE public.post (
    id SERIAL PRIMARY KEY,
    user_id INT REFERENCES public.user(id),
    title VARCHAR(255) NOT NULL,
    content TEXT NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW()
);

CREATE TABLE public.comment (
    id SERIAL PRIMARY KEY,
    post_id INT REFERENCES public.post(id),
    user_id INT REFERENCES public.user(id),
    content TEXT NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW()
);


-- Insert Users
INSERT INTO public.user ( username, email, password)
VALUES
    ( 'alice', 'alice@example.com', '123'),
    ( 'bob', 'bob@example.com',  '123'),
    ( 'charlie', 'charlie@example.com',  '123');

-- Insert Posts
INSERT INTO public.post ( title, content, user_id)
VALUES
    ( 'First Post', 'This is the content of the first post', (SELECT Id FROM public.user WHERE Username = 'alice')),
    ( 'Second Post', 'This is the content of the second post', (SELECT Id FROM public.user WHERE Username = 'bob')),
    ( 'Third Post', 'This is the content of the third post', (SELECT Id FROM public.user WHERE Username = 'charlie'));

-- Insert Comments
INSERT INTO public.comment (content, user_id, post_id)
VALUES
    ('Great post!', (SELECT Id FROM public.user WHERE Username = 'bob'), (SELECT Id FROM public.post WHERE Title = 'First Post')),
    ('Very informative, thanks!', (SELECT Id FROM public.user WHERE Username = 'charlie'), (SELECT Id FROM public.post WHERE Title = 'First Post')),
    ('I totally agree!', (SELECT Id FROM public.user WHERE Username = 'alice'), (SELECT Id FROM public.post WHERE Title = 'Second Post')),
    ('Thanks for sharing!', (SELECT Id FROM public.user WHERE Username = 'bob'), (SELECT Id FROM public.post WHERE Title = 'Third Post'));
